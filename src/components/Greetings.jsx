import React from 'react'

function Greetings(props){
    return (
        <h1>Selamat {props.waktu} {props.nama}</h1>
    )
}

export default Greetings;