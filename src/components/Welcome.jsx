import React from 'react'

class Welcome extends React.Component{
    render(){
        return (
            <h1>Selamat Datang di Class Component {this.props.nama}</h1>
        )
    }
}

export default Welcome;