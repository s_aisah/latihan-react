import React from 'react'
import logo from '../logo.svg';

class Header extends React.Component{
    render(){
        return (
            <div className="App">
            <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h3>Selamat datang di aplikasi {this.props.nama}</h3>
            </header>
            </div>
        )
    }
}

export default Header;