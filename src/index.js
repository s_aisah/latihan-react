import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Header from './components/Header';
import Input from './components/Input';
import Button from './components/Button';
import * as serviceWorker from './serviceWorker';

const cobajsx = <h1>Ais belajar menggunakan jsx</h1>

const element = React.createElement ('p',{className: 'greeting'}, 'Halo Ais, kamu sudah buat element jsx dengan fungsi create element');

const nama = "Siti Aisah"
const jurusan = "Sistem Informasi"
const kampus = "Universitas Brawijaya"
const angkatan = 2016

const ekspresijs = <div>
<h1 className="warna_biru">Halo, perkenalkan nama saya {nama}</h1>
<h2>Saya kuliah di {jurusan+' '.concat(kampus)}</h2>
<h3 style={{color:"red"}}>Saya sudah kuliah selama {2020-angkatan+' tahun'}</h3>
</div>
ReactDOM.render(
  <React.StrictMode>
    <Header nama="React"/>
    <Input place="Username"/>
    <br/>
    <Input place="Password"/>
    <br/>
    <Button/>
  </React.StrictMode>
  ,document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
